/*
|--------------------------------------------------------------------------
| CSS Minification By gulp
|--------------------------------------------------------------------------
*/

var fs = require('fs');
var path = require('path');
var gulp = require('gulp');
var runSequence = require('run-sequence');
var clean = require('gulp-clean');
var sass = require('gulp-sass');
var concat = require('gulp-concat');
var minifycss = require('gulp-minify-css');
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');


gulp.task('clean-sass', function () {
    return gulp.src('css/*.css', {read: false})
        .pipe(clean());
});

// Compile Our Sass
gulp.task('sass', ['clean-sass'], function() {
    return gulp.src('sass/app.scss')
        .pipe(sass())
        .pipe(gulp.dest('css'))
        .pipe(rename({suffix: '.min'}))
        .pipe(minifycss())
        .pipe(gulp.dest('css'));
});

// Final Build
gulp.task('build', function(callback) {
    runSequence('sass',callback);
});

