import React from 'react'

export default class ProgressBar extends React.Component {
  render () {
    var countValue = Math.round(((this.props.barsNodes * 100) / this.props.barLimit) > 0 ? (this.props.barsNodes * 100) / this.props.barLimit : 0)
    return (
      <div className='mainContainer'>
        <p className='title'>Progress bar {this.props.barNumber + 1}</p>
        <div className='progress'>
          <div className='progress-bar' role='progressbar'
            ariaValueNow={countValue}
            ariaValueMin='0'
            ariaValueMax='100'
            style={{'width': countValue + '%', 'maxWidth': '100%', 'backgroundColor': countValue > 100 ? '#ff0000' : '#337ab7'}}>
            <span className='barMidValue' style={{'color': countValue > 55 ? '#ffffff' : '#000000'}}>{countValue}%</span>
          </div>
        </div>
      </div>
    )
  }
}
