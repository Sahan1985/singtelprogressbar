import React from 'react'

export default class ButtonBar extends React.Component {
  render () {
    if (this.props.buttonNodes.length === 0 && this.props.barsNodes.length === 0) {
      return (<div />)
    }

    var barsNodes = this.props.barsNodes.map(function (barsNodes, i) {
      return (
        <option value={i}>ProgressBar {i + 1}</option>
      )
    })
    var currentObj = this.props
    var buttonNodes = this.props.buttonNodes.map(function (buttonNodes) {
      return (
        <li onClick={() => currentObj.changeProgress(buttonNodes)}>{buttonNodes}</li>
      )
    })

    return (
      <div>
        <center>
          <ul className='progressBarBottom'>
            <li>
              <div className='select bottomFilters-sort'>
                <select onChange={event => this.props.changeDropDown(event.target.value)}>
                  {barsNodes}
                </select>
              </div>
            </li>
            {buttonNodes}
          </ul>
        </center>
      </div>
    )
  }
}
