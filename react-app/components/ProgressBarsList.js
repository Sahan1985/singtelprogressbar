import React from 'react'
import ProgressBar from './ProgressBar.js'
import ButtonBar from './ButtonBar.js'

export default class UserList extends React.Component {
  render () {
    if (this.props.users.length === 0) {
      return (<div />)
    }

    var maxLimit = this.props.users.limit

    var barsNodes = this.props.users.bars.map(function (barsNodes, i) {
      return (
        <ProgressBar barsNodes={barsNodes} barLimit={maxLimit} key={i} barNumber={i} />
      )
    })
    return (
      <div className='progressBar'>
        <center>
          <h1 className='mainTitle'>Progress Bar Project</h1>
          {barsNodes}
          <ButtonBar barsNodes={this.props.users.bars} buttonNodes={this.props.users.buttons}
            changeDropDown={this.props.changeDropDown}
            changeProgress={this.props.changeProgress} />
        </center>
      </div>
    )
  }
}
