/* global test, expect */
import {changeDropDown, changeProgress} from './userActions.js'

test('Change Dropdown Action => ', () => {
  expect(changeDropDown(3)).toEqual({
    type: 'CHANGE_DROP_DOWN',
    eventValue: 3
  })
})

test('Change Progress Action => ', () => {
  expect(changeProgress(3)).toEqual({
    type: 'CHANGE_PROGRESS',
    clickValue: 3
  })
})
