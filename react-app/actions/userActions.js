import axios from 'axios'

export const fetchBarsData = () => {
  return (dispatch) => {
    axios.get('http://pb-api.herokuapp.com/bars')
      .then((response) => {
        dispatch({type: 'FETCH_DATA_FULFILLED', payload: response.data})
      })
      .catch((err) => {
        dispatch({type: 'FETCH_DATA_REJECTED', payload: err})
      })
  }
}

export const changeDropDown = (eventValue) => {
  return {
    type: 'CHANGE_DROP_DOWN',
    eventValue
  }
}
export const changeProgress = (clickValue) => {
  return {
    type: 'CHANGE_PROGRESS',
    clickValue
  }
}
