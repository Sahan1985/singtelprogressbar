import React from 'react'
import ReactDOM from 'react-dom'
import {Provider} from 'react-redux'
import MainComponent from './containers/ProgressBarContainer'
import store from './store.js'

ReactDOM.render(
  <Provider store={store}>
    <MainComponent />
  </Provider>,
  document.getElementById('main')
)
