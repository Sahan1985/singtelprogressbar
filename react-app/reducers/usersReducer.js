export default function reducer (state = {
  users: [],
  fetching: false,
  fetched: false,
  error: null,
  selectedBar: 0
}, action) {
  switch (action.type) {
    case 'FETCH_DATA':
      return {
        ...state,
        fetching: true
      }
    case 'FETCH_DATA_FULFILLED':
      return {
        ...state,
        fetching: false,
        fetched: true,
        users: action.payload,
        selectedBar: 0
      }
    case 'FETCH_DATA_REJECTED':
      return {
        ...state,
        fetching: false,
        error: action.payload,
        selectedBar: 0
      }
    case 'CHANGE_DROP_DOWN':
      return {
        ...state,
        selectedBar: parseInt(action.eventValue)
      }
    case 'CHANGE_PROGRESS':
      let currentDataLimit = state.users.limit
      let currentDataButtons = state.users.buttons
      let currentDataBars = state.users.bars
      currentDataBars[state.selectedBar] = (parseInt(currentDataBars[state.selectedBar]) + parseInt(action.clickValue)) > 0
      ? parseInt(currentDataBars[state.selectedBar]) + parseInt(action.clickValue) : 0
      let newData = {'buttons': currentDataButtons, 'bars': currentDataBars, 'limit': currentDataLimit}
      return {
        ...state,
        users: newData
      }
    default:
      return state
  }
}
