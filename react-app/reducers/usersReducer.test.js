/* global test, expect */
import reducer from './usersReducer.js'

test('Reducer [FETCH_DATA] => ', () => {
  expect(reducer(undefined, {type: 'FETCH_DATA'})).toEqual({
    users: [],
    fetching: true,
    fetched: false,
    error: null,
    selectedBar: 0
  })
})

test('Reducer [FETCH_DATA_FULFILLED] => ', () => {
  expect(reducer(undefined, {type: 'FETCH_DATA_FULFILLED',
    payload: {
      'buttons': [10, 38, -13, -18],
      'bars': [62, 45, 62],
      'limit': 230
    }
  })).toEqual({
    users: {
      'buttons': [10, 38, -13, -18],
      'bars': [62, 45, 62],
      'limit': 230
    },
    fetching: false,
    fetched: true,
    error: null,
    selectedBar: 0
  })
})
