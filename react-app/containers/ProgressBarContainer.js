import React from 'react'
import {connect} from 'react-redux'
import ProgressBarsList from '../components/ProgressBarsList'
import * as mainBarsData from '../actions/userActions'

class ProgressBarContainer extends React.Component {
  componentWillMount () {
    this.props.getBarsData()
  }

  render () {
    return (
      <div>
        <ProgressBarsList users={this.props.users}
          changeDropDown={this.props.changeDropDown}
          changeProgress={this.props.changeProgress} />
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    users: state.userReducer.users
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    getBarsData: () => {
      dispatch(mainBarsData.fetchBarsData())
    },
    changeDropDown: (eventValue) => {
      dispatch(mainBarsData.changeDropDown(eventValue))
    },
    changeProgress: (clickValue) => {
      dispatch(mainBarsData.changeProgress(clickValue))
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ProgressBarContainer)
