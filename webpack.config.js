const webpack = require('webpack');

module.exports = {
     entry: './react-app/app.js',
     output: {
         path: './js',
         filename: 'app.bundle.js'
     },
     module: {
         loaders: [{
             test: /\.jsx?$/,
             exclude: /node_modules/,
             loader: 'babel-loader',
             query: {
                presets: ['es2015','react','stage-0'],
                plugins: ['react-html-attrs', 'transform-class-properties','transform-decorators-legacy']
             }
         }]
     },
     plugins:[
       new webpack.DefinePlugin({
         'process.env':{
           'NODE_ENV': JSON.stringify('production')
         }
       }),
       new webpack.optimize.UglifyJsPlugin({
         compress:{
           warnings: true
         }
       })
     ]
 };
