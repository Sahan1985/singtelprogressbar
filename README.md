# ----ReactReduxProgressBarApp----
Sahan Yapa Bandara

###### 1. Run below commands to Sever Up

    npm install
    npm run server

###### 2. Run Below commands to generate CSS minification files
     bower install
     gulp build

###### 3. Run Below commands to generate JS minification files
     npm run build

###### 4. Run Below commands to Test Case
     npm run test

###### 5. Run Below commands to validate javaScript Code Quality
     npm run standard


